<?php
/**
 * @file
 * Contains \Drupal\pradhama_prayoga\Controller\FirstModuleController
 */

namespace Drupal\pradhama_prayoga\Controller;

use Drupal\Core\Controller\ControllerBase;

class FirstModuleController extends ControllerBase {
  public function content(){
   return array(
		'#type'=>'markup',
		'#markup'=>t('Pradhama Prayoga Safalam')
		); 
  }		
}
